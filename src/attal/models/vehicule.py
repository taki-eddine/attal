#***********************************************************************************************************************
import openerp
from openerp import tools
from openerp import models, fields, api
#***********************************************************************************************************************

class Vehicule(models.Model):
    _name = "attal.vehicule"

    name  = fields.Char(string = "Mark", required = True)
    image = fields.Binary(string = "Image", default = lambda self: self._get_default_image())
    year  = fields.Date(string = "Year")

    # specs
    type       = fields.Char(string = "Type")
    drivetrain = fields.Selection(string = "Drivetrain", selection = [("4x4", "4x4"), ("4x2", "4x2")])
    suspension = fields.Selection(string = "Suspension", selection = [("solid_beam_axle", "Solid Beam Axle"),
                                                                      ("swing_axle", "Swing Axle")])
    cabine     = fields.Integer()
    length     = fields.Char()
    mines      = fields.Selection(string = "Mines", selection = [(0, "No"), (1, "Yes")])
    power      = fields.Integer()

    ## Read the default image.
    # @return image
    @api.model
    def _get_default_image(self):
        img_path = openerp.modules.get_module_resource("attal", "static/src/img", "vehicule.png")
        return tools.image_resize_image_big(open(img_path).read().encode("base64"))
