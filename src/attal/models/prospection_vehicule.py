#***********************************************************************************************************************
from openerp import models, fields, api
#***********************************************************************************************************************

class ProspectorVehicule(models.Model):
    _name = 'attal.prospection_vehicule'

    # needed because of the `one2many` field.
    propection_id = fields.Many2one('attal.prospection', string = "Prospector", required = True)

    # vehicule
    vehicule_id = fields.Many2one("attal.vehicule", string = "Vehicule", required = True)
    mileage     = fields.Integer(string = "Mileage")
    # TODO: use `fields.Monetary`.
    price       = fields.Float(string = "Price", digits = (6, 3))
    notes       = fields.Text(string = "Notes")
