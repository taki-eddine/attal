#***********************************************************************************************************************
from openerp import models, fields, api
#***********************************************************************************************************************

class Prospection(models.Model):
    _name = 'attal.prospection'

    user_id                  = fields.Many2one("res.users", string = "Prospector", default = lambda self: self.env.user.id)
    name                     = fields.Id()
    provider_id              = fields.Many2one('attal.provider', string = "Provider", required = True)
    prospection_vehicule_ids = fields.One2many("attal.prospection_vehicule", "propection_id", string = "Prospection's Vehicules")
