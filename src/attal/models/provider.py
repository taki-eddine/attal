#***********************************************************************************************************************
import openerp
from openerp import tools
from openerp import models, fields, api
#***********************************************************************************************************************

class Provider(models.Model):
    _name = 'attal.provider'

    name  = fields.Char(string = "Name", required = True)
    image = fields.Binary(string = "Image", default = lambda self: self._get_default_image())

    # adress
    street     = fields.Char(string = "Street")
    city       = fields.Char(string = "City")
    zip        = fields.Char(string = "ZIP")
    country_id = fields.Many2one("res.country", "Country", ondelete = "restrict")

    phone    = fields.Char(string = "Phone")
    fax      = fields.Char(string = "Fax")
    web_site = fields.Char(string = "Web Site")

    # contact
    contact = fields.Char(string = "Contact")
    mobile  = fields.Char(string = "Mobile")
    email   = fields.Char(string = "Email")
    notes   = fields.Text(string = "Notes")

    ## Read the default image.
    # @return image
    @api.model
    def _get_default_image(self):
        img_path = openerp.modules.get_module_resource("attal", "static/src/img", "provider.png")
        return tools.image_resize_image_big(open(img_path).read().encode("base64"))
