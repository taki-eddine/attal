# -*- coding: utf-8 -*-
{
    'name': "Attal",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com
    """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Attal",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Tools',
    'version': '0.1',

    # options
    'installable': True,
    'application': True,

    # any module necessary for this one to work correctly
    'depends': [
        'base'
    ],

    # always loaded
    'data': [
        'security/attal_security.xml',
        'security/ir.model.access.csv',
        'views/prospection.xml',
        'views/prospection_vehicule.xml',
        'views/provider.xml',
        'views/vehicule.xml',
        'views/menus.xml',
    ],

    'qweb': [
    ],

    # only loaded in demonstration mode
    'demo': [
    ],
}