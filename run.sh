#!/usr/bin/bash

# clean
find ./src/attal -name '*.pyc' -type f -exec rm '{}' \;

# run
odoo.py --config="/etc/odoo/odoo.conf" --workers=$(nproc) --max-cron-threads=$(nproc)
